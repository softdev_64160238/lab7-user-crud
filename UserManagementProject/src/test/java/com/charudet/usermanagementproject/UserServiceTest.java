/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.charudet.usermanagementproject;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class UserServiceTest {
    UserService userService;
    
    public UserServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userService = new UserService();
        User newAdmin1 = new User("admin1", "Administrator", "Pass@1234", 'M', 'A');
        User newAdmin2 = new User("admin2", "Administrator", "Pass@1234", 'F', 'A');
        User newUser1 = new User("user1", "User 1", "Pass@1234", 'M', 'U');
        User newUser2 = new User("user2", "User 2", "Pass@1234", 'F', 'U');
        userService.addUser(newAdmin1);
        userService.addUser(newAdmin2);
        userService.addUser(newUser1);
        userService.addUser(newUser2);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class UserService.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User newAdmin = new User("admin", "Administrator", "Pass@1234", 'M', 'A');
        UserService instance = new UserService();
        User expResult = newAdmin;
        User result = instance.addUser(newAdmin);
        assertEquals(1, result.getId());
    }

    /**
     * Test of getUser method, of class UserService.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        int index = 1;        
        String expResult = "admin2";
        User result = userService.getUser(index);
        assertEquals(expResult, result.getLogin());
    }

    /**
     * Test of getUsers method, of class UserService.
     */
    @Test
    public void testGetUsers() {
        System.out.println("getUsers");
        ArrayList<User> userList = userService.getUsers();
        assertEquals(4, userList.size());
    }

    /**
     * Test of getSize method, of class UserService.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 4;
        int result = userService.getSize();
        assertEquals(expResult, result);
    }
    
}
