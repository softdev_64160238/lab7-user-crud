/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.charudet.usermanagementproject;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class UserManagementProject {

    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "pass@1234", 'M', 'A');
        User user1 = new User("user1", "User 1", "pass@1234", 'M', 'U');
        User user2 = new User("user2", "User 2", "pass@1234", 'F', 'U');
        User user3 = new User("user3", "User 3", "pass@1234", 'F', 'U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("");
        System.out.println("Print for userArr");
        for (int i = 0; i < userArr.length; i++) {
            System.out.println(userArr[i]);
        }
        
        ArrayList<User> userList = new ArrayList<User>();
        userList.add(admin);
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        System.out.println("");
        System.out.println("ArrayList");
        for (int i = 0; i < userList.size(); i++) {
            System.out.println(userList.get(i));
        }
    }
}

